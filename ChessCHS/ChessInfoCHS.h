#pragma once

namespace Wzz
{
	class CChessInfoCHS
	{
	private:
		enum ChessType
		{
			CHESSTYPE_NULL         = -1,
			CHESSTYPE_RED_JU       = 0,
			CHESSTYPE_RED_MA       = 1,
			CHESSTYPE_RED_PAO      = 2,
			CHESSTYPE_RED_SHUAI    = 3,
			CHESSTYPE_RED_SHI      = 4,
			CHESSTYPE_RED_XIANG    = 5,
			CHESSTYPE_RED_BING     = 6,
			CHESSTYPE_BLACK_JU     = 7,
			CHESSTYPE_BLACK_MA     = 8,
			CHESSTYPE_BLACK_PAO    = 9,
			CHESSTYPE_BLACK_JIANG  = 10,
			CHESSTYPE_BLACK_SHI    = 11,
			CHESSTYPE_BLACK_XIANG  = 12,
			CHESSTYPE_BLACK_ZU     = 13
		};
	private:
		enum ChessGameType
		{
			CHESSGAMETYPE_PERSON2PERSON   = 0,
			CHESSGAMETYPE_PERSON2MYSELF   = 1,
			CHESSGAMETYPE_PERSON2COMPUTER = 2
		};
	public:
		CChessInfoCHS(void);
		~CChessInfoCHS(void);

	public:
		// 初始化象棋棋子数据
		void InitChessInfo(void);
		void ClearChessInfo(void);
		void DrawAllChess(CDC* pDC);
		void DrawChessTile(CDC* pDC);
		bool GetGameStart() const;
		void SetGameStart(const bool bGameStart);
		bool ChessClicked(const CPoint point);
		CPoint GetChessFocusStart() const;
		CPoint GetChessFocusStop() const;
		void SetChessFocusStart(const CPoint pointStart);
		void SetChessFocusStop(const CPoint pointStop);
		static CRect GetCRectChessCHS(const CPoint point);
		static CPoint GetCPointChessCHS(const CPoint point);
		static CPoint TranslateCPoint(const CPoint pointChess);

		bool GetMySelfRun() const;
		void SetMySelfRun(const bool bMySelfRun);

		bool ChessMoveOrEat(const CPoint pointBegin, const CPoint pointEnd);
		bool CanChessMoveOrEat(const CPoint pointBegin, const CPoint pointEnd) const;
		bool CanMoveChessJU(const CPoint pointBegin, const CPoint pointEnd) const;
		bool CanMoveChessMA(const CPoint pointBegin, const CPoint pointEnd) const;
		bool CanMoveChessPAO(const CPoint pointBegin, const CPoint pointEnd) const;
		bool CanMoveChessSHI(const CPoint pointBegin, const CPoint pointEnd) const;
		bool CanMoveChessKING(const CPoint pointBegin,const CPoint pointEnd) const;
		bool CanMoveChessBING(const CPoint pointBegin,const CPoint pointEnd) const;
		bool CanMoveChessXIANG(const CPoint pointBegin, const CPoint pointEnd) const;

	public:
		bool IsChessKing(const CPoint point) const;
	private:
		bool IsChessRed(const CPoint point) const;
		bool IsChessBlack(const CPoint point) const;
		bool IsChessNull(const CPoint point) const;

	private:
		CPoint m_ChessCHSFocusStart;
		CPoint m_ChessCHSFocusStop;
		ChessType m_ChessCHS[9][10];                  // 棋子在棋盘上的布局
		static const CPoint s_CpChessPosition[9][10]; // 棋盘下子处的各个像素坐标

	private:
		bool m_bGameStart;
		bool m_bGameSelf;
	};
}
