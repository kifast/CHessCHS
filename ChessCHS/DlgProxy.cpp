
// DlgProxy.cpp : 实现文件
//

#include "stdafx.h"
#include "ChessCHS.h"
#include "DlgProxy.h"
#include "ChessCHSDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChessCHSDlgAutoProxy

IMPLEMENT_DYNCREATE(CChessCHSDlgAutoProxy, CCmdTarget)

CChessCHSDlgAutoProxy::CChessCHSDlgAutoProxy()
{
	EnableAutomation();
	
	// 为使应用程序在自动化对象处于活动状态时一直保持 
	//	运行，构造函数调用 AfxOleLockApp。
	AfxOleLockApp();

	// 通过应用程序的主窗口指针
	//  来访问对话框。设置代理的内部指针
	//  指向对话框，并设置对话框的后向指针指向
	//  该代理。
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CChessCHSDlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CChessCHSDlg)))
		{
			m_pDialog = reinterpret_cast<CChessCHSDlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CChessCHSDlgAutoProxy::~CChessCHSDlgAutoProxy()
{
	// 为了在用 OLE 自动化创建所有对象后终止应用程序，
	// 	析构函数调用 AfxOleUnlockApp。
	//  除了做其他事情外，这还将销毁主对话框
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CChessCHSDlgAutoProxy::OnFinalRelease()
{
	// 释放了对自动化对象的最后一个引用后，将调用
	// OnFinalRelease。基类将自动
	// 删除该对象。在调用该基类之前，请添加您的
	// 对象所需的附加清理代码。

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CChessCHSDlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CChessCHSDlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// 注意: 我们添加了对 IID_IChessCHS 的支持
//  以支持来自 VBA 的类型安全绑定。此 IID 必须同附加到 .IDL 文件中的
//  调度接口的 GUID 匹配。

// {F47DC582-AACD-408E-878C-3CFA384547CC}
static const IID IID_IChessCHS =
{ 0xF47DC582, 0xAACD, 0x408E, { 0x87, 0x8C, 0x3C, 0xFA, 0x38, 0x45, 0x47, 0xCC } };

BEGIN_INTERFACE_MAP(CChessCHSDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CChessCHSDlgAutoProxy, IID_IChessCHS, Dispatch)
END_INTERFACE_MAP()

// IMPLEMENT_OLECREATE2 宏在此项目的 StdAfx.h 中定义
// {C484429F-AAA7-4E5E-B7C2-604932D3EEE9}
IMPLEMENT_OLECREATE2(CChessCHSDlgAutoProxy, "ChessCHS.Application", 0xc484429f, 0xaaa7, 0x4e5e, 0xb7, 0xc2, 0x60, 0x49, 0x32, 0xd3, 0xee, 0xe9)


// CChessCHSDlgAutoProxy 消息处理程序
