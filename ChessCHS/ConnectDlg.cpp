// ConnectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ChessCHS.h"
#include "ConnectDlg.h"
#include "afxdialogex.h"


// CConnectDlg 对话框

IMPLEMENT_DYNAMIC(CConnectDlg, CDialogEx)

CConnectDlg::CConnectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CConnectDlg::IDD, pParent)
{

}

CConnectDlg::~CConnectDlg()
{
}

void CConnectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CConnectDlg, CDialogEx)
END_MESSAGE_MAP()


// CConnectDlg 消息处理程序
