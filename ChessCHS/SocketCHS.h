#pragma once


// CSocketCH 命令目标

#include <map>

#include <list>

#include <string>


namespace Wzz
{
#define MULTICAST_IPADDR "225.1.1.1"
#define MULTICAST_PORT   78911
#define BROADCAST_PORT   78912

	class CUDPSocketCHS : public CAsyncSocket
	{
	public:
		enum UdpMessageType
		{
			UDPMESSAGETYPE_CHATMSG,
			UDPMESSAGETYPE_USERLIST_LINEON,
			UDPMESSAGETYPE_USERLIST_LINEOUT,
			UDPMESSAGETYPE_USERLIST_REQUEST,
			UDPMESSAGETYPE_USERLIST_RESPONSE
		};
		struct UdpMessagePkt
		{
			UdpMessageType msgType;
			GUID guid;       // 因为是会对多个网段进行广播，所以添加一个GUID标识，避免重复显示数据
			union
			{
				char szMsg[200];
				struct userInfo
				{
					char szUserName[20];
				};
			};
		};
		enum UdpSocketType
		{
			UDPSOCKETTYPE_BROADCAST,
			UDPSOCKETTYPE_MULTICAST,
			UDPSOCKETTYPE_DEFAULT
		};

	public:
		CUDPSocketCHS(const UdpSocketType type);
		virtual ~CUDPSocketCHS();
		virtual void OnReceive(int nErrorCode);

		static GUID CreateNewGuid();
		static bool ExistsRecvGuidList(const GUID guid);

		static bool MakeUdpMessagePkt(char* const pszDstUdpMessage, const int nDstUdpMessageSize, const UdpMessagePkt& srcUdpMessagePkt);
		static bool ParseUdpMessagePkt(UdpMessagePkt& dstUdpMessagePkt, const char* const pszSrcUdpMessage, const int nSrcUdpMessageLen);

	public:
		void SendTo(const UdpMessagePkt udpMessagePkt, int nFlags = 0);

	private:
		static void InsertRecvGuidList(const GUID guid);
		void GetAllAdapterInfo();

	private:
		static std::list<const GUID> m_ListRecvGuid;
		std::map<std::string, std::string> m_MapIPNet;
		CMutex m_CMutexMap;
		static CMutex s_CMutexList;
		UdpSocketType m_UdpSocketType;
	};


	class CTCPSocketCHS : public CAsyncSocket
	{
	public:
		enum TcpMessageType
		{
			TCPMESSAGETYPE_USERINFO_REQUEST,
			TCPMESSAGETYPE_USERINFO_RESPONSE,

			TCPMESSAGETYPE_GAMESTART_REQUEST,
			TCPMESSAGETYPE_GAMESTART_AGREE,
			TCPMESSAGETYPE_GAMESTART_REFUSE,

			TCPMESSAGETYPE_GAMEHUIQI_REQUEST,
			TCPMESSAGETYPE_GAMEHUIQI_AGREE,
			TCPMESSAGETYPE_GAMEHUIQI_REFUSE,

			TCPMESSAGETYPE_GAMEQIUHE_REQUEST,
			TCPMESSAGETYPE_GAMEQIUHE_AGREE,
			TCPMESSAGETYPE_GAMEQIUHE_REFUSE,

			TCPMESSAGETYPE_GAMERENSHU_REQUEST,
			TCPMESSAGETYPE_GAMERENSHU_RESPONSE,

			TCPMESSAGETYPE_GAMEPLAY_REQUEST,
			TCPMESSAGETYPE_GAMEPLAY_RESPONSE,

			TCPMESSAGETYPE_GAMEWIN_REQUEST,
			TCPMESSAGETYPE_GAMEWIN_RESPONSE,

			TCPMESSAGETYPE_GAMEEXIT_MESSAGE   // 对方退出游戏的消息
		};
		struct TcpMessagePkt
		{
			TcpMessageType msgType;
			union
			{
				struct
				{
					CPoint pointStart;
					CPoint pointStop;
				};
				struct
				{
					char szUserName[30];
					char szPCUserName[50];
				};
			};
		};

	public:
		CTCPSocketCHS();
		virtual ~CTCPSocketCHS();
		virtual void OnReceive(int nErrorCode);
		virtual void OnConnect(int nErrorCode);
		virtual void OnAccept(int nErrorCode);

	public:
		int Send(const TcpMessagePkt tcpMessagePkt, int nFlags = 0);
		static bool MakeTcpMessagePkt(char* const pszDstTcpMessage, const int nDstTcpMessageSize, const TcpMessagePkt& srcTcpMessagePkt);
		static bool ParseTcpMessagePkt(TcpMessagePkt& dstTcpMessagePkt, const char* const pszSrcTcpMessage, const int nSrcTcpMessageLen);
	};
}
