// SocketCH.cpp : 实现文件
//

#include "stdafx.h"
#include "ChessCHS.h"
#include "SocketCHS.h"

#include "ChessCHSDlg.h"

#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")

#include <winsock2.h>
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

#define MALLOC(x) HeapAlloc(GetProcessHeap(),0,(x))
#define FREE(x) if(NULL!=x){HeapFree(GetProcessHeap(),0,(x));x=NULL;}



namespace Wzz
{
	// CSocketCH
	CMutex CUDPSocketCHS::s_CMutexList(FALSE);
	std::list<const GUID> CUDPSocketCHS::m_ListRecvGuid;

	CUDPSocketCHS::CUDPSocketCHS(const UdpSocketType type)
		: m_CMutexMap(FALSE)
		, m_UdpSocketType(type)
	{
		GetAllAdapterInfo();
	}

	CUDPSocketCHS::~CUDPSocketCHS()
	{
	}

	GUID CUDPSocketCHS::CreateNewGuid()
	{
		GUID guid;
		UuidCreate(&guid);

		return guid;
	}

	// CSocketCH 成员函数

	void CUDPSocketCHS::OnReceive(int nErrorCode)
	{
		// TODO: 在此添加专用代码和/或调用基类
		((CChessCHSDlg*)AfxGetMainWnd())->OnUDPReceive(m_UdpSocketType, nErrorCode);

		CAsyncSocket::OnReceive(nErrorCode);
	}


	// 获取所有网卡的信息
	void CUDPSocketCHS::GetAllAdapterInfo()
	{
		m_CMutexMap.Lock();

		m_MapIPNet.clear();

		ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);
		PIP_ADAPTER_INFO pAdapterInfo = (PIP_ADAPTER_INFO)MALLOC(sizeof(IP_ADAPTER_INFO));
		if (NULL == pAdapterInfo)
		{
			return;
		}
		// Make an initial call to GetAdaptersInfo to get
		// the necessary size into the ulOutBufLen variable
		else if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			FREE(pAdapterInfo);
			pAdapterInfo = (PIP_ADAPTER_INFO)MALLOC(ulOutBufLen);
			if (NULL == pAdapterInfo)
			{
				return;
			}
		}

		if (NULL!=pAdapterInfo && NO_ERROR==GetAdaptersInfo(pAdapterInfo, &ulOutBufLen))
		{
			PIP_ADAPTER_INFO pAdapter = pAdapterInfo;

			while (NULL != pAdapter)
			{
				PIP_ADDR_STRING pIPAddress = &(pAdapter->IpAddressList);

				while (NULL != pIPAddress) 
				{
					// --过滤掉没有IP地址的网卡
					std::string strIP(pIPAddress->IpAddress.String);
					std::string strMask(pIPAddress->IpMask.String);
					if (std::string("0.0.0.0") != strIP)
					{
						unsigned long ulIP = ntohl(inet_addr(pIPAddress->IpAddress.String));
						unsigned long ulMask = ntohl(inet_addr(pIPAddress->IpMask.String));
						in_addr inAddr = {0};
						inAddr.S_un.S_addr = htonl(ulIP | (~ulMask));
						std::string strNet(inet_ntoa(inAddr));

						m_MapIPNet.insert(make_pair(strIP, strNet));
					}

					pIPAddress = pIPAddress->Next;
				};

				pAdapter = pAdapter->Next;
			}
		}
		FREE(pAdapterInfo);

		m_CMutexMap.Unlock();
	}


	void CUDPSocketCHS::SendTo(const UdpMessagePkt udpMessagePkt, int nFlags/*=1*/)
	{
		InsertRecvGuidList(udpMessagePkt.guid);
		char szUdpMessageBuffer[1500] = {'\0'};
		if (MakeUdpMessagePkt(szUdpMessageBuffer, sizeof(szUdpMessageBuffer), udpMessagePkt))
		{
			if (UDPSOCKETTYPE_BROADCAST == m_UdpSocketType)
			{
				for (std::map<std::string, std::string>::iterator pIter = m_MapIPNet.begin();
					pIter != m_MapIPNet.end();
					pIter++)
				{
					CAsyncSocket::SendTo(szUdpMessageBuffer, sizeof(UdpMessagePkt), BROADCAST_PORT, CA2W(pIter->second.c_str()), nFlags);
				}
			} 
			else if (UDPSOCKETTYPE_MULTICAST == m_UdpSocketType)
			{
				CAsyncSocket::SendTo(szUdpMessageBuffer, sizeof(UdpMessagePkt), MULTICAST_PORT, CA2W(MULTICAST_IPADDR), nFlags);
			}
		}
	}


	// GUID是否存在
	bool CUDPSocketCHS::ExistsRecvGuidList(const GUID guid)
	{
		bool bFind = false;

		s_CMutexList.Lock();
		for (std::list<const GUID>::iterator pIter = m_ListRecvGuid.begin();
			pIter != m_ListRecvGuid.end();
			pIter++)
		{
			if (guid == *pIter)
			{
				bFind = true;
			}
		}

		if (!bFind)
		{
			InsertRecvGuidList(guid);
		}

		s_CMutexList.Unlock();

		return bFind;
	}

	void CUDPSocketCHS::InsertRecvGuidList(const GUID guid)
	{
		m_ListRecvGuid.push_back(guid);
		
		if (30 < m_ListRecvGuid.size())
		{
			m_ListRecvGuid.pop_front();
		}
	}

	bool CUDPSocketCHS::MakeUdpMessagePkt(char* const pszDstUdpMessage, const int nDstUdpMessageSize, const UdpMessagePkt& srcUdpMessagePkt)
	{
		if (nDstUdpMessageSize >= sizeof(UdpMessagePkt))
		{
			if (0 == memmove_s(pszDstUdpMessage, nDstUdpMessageSize, &srcUdpMessagePkt, sizeof(UdpMessagePkt)))
			{
				return true;
			}
		}

		return false;
	}

	bool CUDPSocketCHS::ParseUdpMessagePkt(UdpMessagePkt& dstUdpMessagePkt, const char* const pszSrcUdpMessage, const int nSrcUdpMessageLen)
	{
		if (nSrcUdpMessageLen != sizeof(UdpMessagePkt))
		{
			return false;
		}
		else
		{
			if (0 == memmove_s(&dstUdpMessagePkt, sizeof(UdpMessagePkt), pszSrcUdpMessage, nSrcUdpMessageLen))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}












	CTCPSocketCHS::CTCPSocketCHS()
	{
	}

	CTCPSocketCHS::~CTCPSocketCHS()
	{
	}


	// CSocketCH 成员函数


	void CTCPSocketCHS::OnReceive(int nErrorCode)
	{
		// TODO: 在此添加专用代码和/或调用基类
		((CChessCHSDlg*)AfxGetMainWnd())->OnTCPReceive(nErrorCode);

		CAsyncSocket::OnReceive(nErrorCode);
	}



	void CTCPSocketCHS::OnConnect(int nErrorCode)
	{
		// TODO: 在此添加专用代码和/或调用基类
		((CChessCHSDlg*)AfxGetMainWnd())->OnTCPConnect(nErrorCode);

		CAsyncSocket::OnConnect(nErrorCode);
	}


	void CTCPSocketCHS::OnAccept(int nErrorCode)
	{
		// TODO: 在此添加专用代码和/或调用基类
		((CChessCHSDlg*)AfxGetMainWnd())->OnTCPAccept(nErrorCode);

		CAsyncSocket::OnAccept(nErrorCode);
	}

	int CTCPSocketCHS::Send(const TcpMessagePkt tcpMessagePkt, int nFlags/*=0*/)
	{
		char szTcpMessageBuffer[1500] = {'\0'};
		if (MakeTcpMessagePkt(szTcpMessageBuffer, sizeof(szTcpMessageBuffer), tcpMessagePkt))
		{
			return CAsyncSocket::Send(szTcpMessageBuffer, sizeof(TcpMessagePkt));
		}
		else
		{
			return SOCKET_ERROR;
		}
	}


	bool CTCPSocketCHS::MakeTcpMessagePkt(char* const pszDstTcpMessage, const int nDstTcpMessageSize, const TcpMessagePkt& srcTcpMessagePkt)
	{
		if (nDstTcpMessageSize >= sizeof(TcpMessagePkt))
		{
			if (0 == memmove_s(pszDstTcpMessage, nDstTcpMessageSize, &srcTcpMessagePkt, sizeof(TcpMessagePkt)))
			{
				return true;
			}
		}

		return false;
	}

	bool CTCPSocketCHS::ParseTcpMessagePkt(TcpMessagePkt& dstTcpMessagePkt, const char* const pszSrcTcpMessage, const int nSrcTcpMessageLen)
	{
		if (nSrcTcpMessageLen == sizeof(TcpMessagePkt))
		{
			OutputDebugString(L"ParseTcpMessagePkt == ");
			if (0 == memmove_s(&dstTcpMessagePkt, sizeof(TcpMessagePkt), pszSrcTcpMessage, nSrcTcpMessageLen))
			{
				OutputDebugString(L"ParseTcpMessagePkt ok");
				return true;
			}
		}
		CString cs;
		cs.Format(L"struct:%d recvlen:%d", sizeof(TcpMessagePkt), nSrcTcpMessageLen);
		OutputDebugString(cs+L"ParseTcpMessagePkt NO");
		return false;
	}
}
