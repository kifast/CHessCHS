

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Tue Mar 19 08:12:45 2013
 */
/* Compiler settings for ChessCHS.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __ChessCHS_h_h__
#define __ChessCHS_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IChessCHS_FWD_DEFINED__
#define __IChessCHS_FWD_DEFINED__
typedef interface IChessCHS IChessCHS;
#endif 	/* __IChessCHS_FWD_DEFINED__ */


#ifndef __ChessCHS_FWD_DEFINED__
#define __ChessCHS_FWD_DEFINED__

#ifdef __cplusplus
typedef class ChessCHS ChessCHS;
#else
typedef struct ChessCHS ChessCHS;
#endif /* __cplusplus */

#endif 	/* __ChessCHS_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __ChessCHS_LIBRARY_DEFINED__
#define __ChessCHS_LIBRARY_DEFINED__

/* library ChessCHS */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_ChessCHS;

#ifndef __IChessCHS_DISPINTERFACE_DEFINED__
#define __IChessCHS_DISPINTERFACE_DEFINED__

/* dispinterface IChessCHS */
/* [uuid] */ 


EXTERN_C const IID DIID_IChessCHS;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("F47DC582-AACD-408E-878C-3CFA384547CC")
    IChessCHS : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct IChessCHSVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IChessCHS * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IChessCHS * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IChessCHS * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IChessCHS * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IChessCHS * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IChessCHS * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IChessCHS * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IChessCHSVtbl;

    interface IChessCHS
    {
        CONST_VTBL struct IChessCHSVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IChessCHS_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IChessCHS_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IChessCHS_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IChessCHS_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IChessCHS_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IChessCHS_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IChessCHS_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __IChessCHS_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ChessCHS;

#ifdef __cplusplus

class DECLSPEC_UUID("C484429F-AAA7-4E5E-B7C2-604932D3EEE9")
ChessCHS;
#endif
#endif /* __ChessCHS_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


